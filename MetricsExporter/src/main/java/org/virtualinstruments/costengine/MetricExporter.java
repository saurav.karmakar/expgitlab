package org.virtualinstruments.costengine;

import lombok.SneakyThrows;
import org.virtualinstruments.costengine.models.MetricValues;
import org.virtualinstruments.costengine.service.MetricsExporter;
import org.virtualinstruments.costengine.service.MetricsExporterImpl;
import software.amazon.awssdk.services.cloudwatch.CloudWatchClient;
import software.amazon.awssdk.services.cloudwatch.model.StandardUnit;

import java.util.HashMap;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MetricExporter {
    private final Timer timer;
    private final Random random;

    public MetricExporter() {
        timer = new Timer();
        random = new Random();
    }

    public void startScheduler() {
        timer.schedule(new Task(), 0,
                60 *
                        1000); // Run task every 5 minutes (5 * 60 * 1000 milliseconds)
    }

    public void stopScheduler() {
        timer.cancel();
    }

    class Task extends TimerTask {
        @Override
        public void run() {
            double randomValue = random.nextDouble() + random.nextDouble();
            System.out.println(
                    "Random double: " + String.format("%.2f", randomValue));
            // Perform your desired task here
            CloudWatchClient cloudWatchClient = CloudWatchClient.create();

            // SINGLE METRIC EXPORT
            /*MetricsExporter metricsExporter =
                    new MetricsExporterImpl(
                            cloudWatchClient,
                            "SITE/TRAFFIC",
                            MetricValues.builder()
                                    .metricName("PAGES_LOGGED")
                                    .value(randomValue)
                                    .standardUnit(StandardUnit.COUNT)
                                    .build());

            metricsExporter.exportMetric();*/

            // METRIC WITH DIMENSIONS
            HashMap<String, String> dimension = new HashMap<String, String>() {
                {
                    put("UNIQUE_PAGES", "URLS");
                }
            };

            MetricsExporter metricsExporter =
                    new MetricsExporterImpl(
                            cloudWatchClient,
                            "SITE/TRAFFIC",
                            MetricValues.builder()
                                    .metricName("PAGES_BROWSED")
                                    .value(randomValue)
                                    .standardUnit(StandardUnit.COUNT)
                                    .dimensions(dimension)
                                    .build());

            metricsExporter.exportMetricWithDimensions();
        }
    }

    @SneakyThrows
    public static void main(String[] args) {
        MetricExporter scheduler = new MetricExporter();
        scheduler.startScheduler();
    }
}
