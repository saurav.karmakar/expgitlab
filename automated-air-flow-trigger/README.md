# Automated Airflow Trigger

## Run 
Go Run Command <br/>
```
go run *.go DEV session=0565c85f-1f07-47d4-b1bf-0e682fa1fc48.6hldZhClM4-ZUKtUUA-b1Kt79mU
```

## AirFlow REST APIs Reference
https://airflow.apache.org/docs/apache-airflow/stable/stable-rest-api-ref.html

## Endpionts
```
# API : / or /health 
curl --location 'http://localhost:8080/health'
```
```
# API : /listAllDags 
curl --location 'http://localhost:8080/listAllDags'
```
```
# API : /triggerUsageBackfill
curl --location 'http://localhost:8080/triggerUsageBackfill' \
--header 'Content-Type: application/json' \
--data '{
    "orgs": [
        "c1b1bee2-4171-4f9e-b778-0093ca6df6d2",
        "75ec71ee-7260-44a0-9759-ca688b659d1b"
    ],
    "months": 1,
    "cloud": "aws"
}'
```