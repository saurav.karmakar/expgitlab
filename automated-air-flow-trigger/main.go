package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/joho/godotenv"
)

var (
	env          string
	session      string
	execMetadata *ExecutionMetadata
)

func init() {
	logInfo("---- Automated airflow trigger REST Endpoints ----")
	logInfo("")
	logInfo("Initialization...")
	logInfo("loading the env variables")

	// load .env file
	err := godotenv.Load(".env")

	if err != nil {
		logError("Error loading .env file")
		panic(err)
	}
}

// Setting up the execution metadata
func setupExecutionMetadata(env string, session string) (*ExecutionMetadata, error) {

	// check env shouldn't be nil
	if env == "" {
		panic(errors.New("environment can't be blank"))
	}

	switch strings.ToLower(env) {
	case "dev":
		return &ExecutionMetadata{
			Env:         "dev",
			AirFlowHost: os.Getenv("DEV_AIRFLOW_HOST"),
			Session:     session,
		}, nil
	case "prod":
		return &ExecutionMetadata{
			Env:         "prod",
			AirFlowHost: os.Getenv("PROD_AIRFLOW_HOST"),
			Session:     session,
		}, nil
	default:
		return nil, errors.New("unidentified environment, please provide a vaild one. [dev, prod]")
	}
}

// -------- MAIN Function -------- //
func main() {

	logInfo(fmt.Sprintf("total command line args, len: %d", len(os.Args)))
	if len(os.Args) < 3 {
		logMesg := "provided no. of args are less, pclearlease privde env & sessions"
		logError(logMesg)
		panic(errors.New(logMesg))
	}

	for i, arg := range os.Args[1:] {
		logInfo(fmt.Sprintf("command line args: %d:%s", i, arg))
	}

	env = os.Args[1]
	session = os.Args[2]
	execMetadata, _ = setupExecutionMetadata(env, session)

	// Handler function for a specific route.
	http.HandleFunc("/", healthHandler)
	http.HandleFunc("/health", healthHandler)
	http.HandleFunc("/listAllDags", getListOfAllDAGs)
	http.HandleFunc("/triggerUsageBackfill", trigggerUsageBackfill)

	// Start the HTTP server on port 8080.
	logInfo("")
	logInfo(fmt.Sprintf("Server is listening on port %d...\n", PORT))
	err := http.ListenAndServe(fmt.Sprintf(":%d", PORT), nil)
	if err != nil {
		fmt.Println("Error starting server:", err)
	}
}
