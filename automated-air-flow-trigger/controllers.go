package main

import (
	"encoding/json"
	"net/http"
)

// Route : "/ and /health"
func healthHandler(w http.ResponseWriter, r *http.Request) {
	logInfo("API Call: '/' or '/health'")
	health := &Health{
		Version:      "v1.0",
		Message:      "Automated Air Flow Triggers, using the REST API",
		ExecMetadata: *execMetadata,
	}
	response, err := json.Marshal(health)
	if err != nil {
		resp := SetErrorResponse(http.StatusInternalServerError, string("error while marshaling the health response"))
		w.Write(resp)
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(response)
}

// Route : "/listAllDags"
func getListOfAllDAGs(w http.ResponseWriter, r *http.Request) {
	logInfo("API Call: '/listAllDags'")
	dagsLst, err := getAirFlowExecutor(execMetadata).ListAllDAGs()
	if err != nil {
		resp := SetErrorResponse(http.StatusInternalServerError, string("error while fetching the DAGs list of AirFlow"))
		w.Write(resp)
	}
	resp, _ := json.Marshal(dagsLst)
	w.Header().Set("Content-Type", "application/json")
	w.Write(resp)
}

// Trigger backfill for all provided orgs as parameter
func trigggerUsageBackfill(w http.ResponseWriter, r *http.Request) {
	logInfo("API Call: '/triggerUsageBackfill'")
	var reqBody TriggerUsageBackfillReqBody
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		resp := SetErrorResponse(http.StatusBadRequest, string("request payload is invalid"))
		w.Write(resp)
	}

	consolidatedUsageBackfillRes := getAirFlowExecutor(execMetadata).TriggerUsageBackfill(reqBody)
	resp, _ := json.Marshal(consolidatedUsageBackfillRes)
	w.Header().Set("Content-Type", "application/json")
	w.Write(resp)
}
