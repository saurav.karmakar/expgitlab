package main

type AirFlowExecutor interface {
	ListAllDAGs() (DAGSList, error)
	TriggerUsageBackfill(TriggerUsageBackfillReqBody) ConsolidatedUsageBackfillRes
}

func getAirFlowExecutor(executionMetadata *ExecutionMetadata) AirFlowExecutor {
	return &AirFlowExecutorImpl{
		ExeMetadata: executionMetadata,
	}
}
