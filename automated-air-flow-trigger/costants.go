package main

const (
	// Server PORT
	PORT = 8080

	// Endpoints
	ALL_DAGS    = "%s/api/v1/dags"
	TRIGGER_DAG = "%s/api/v1/dags/usage_ingestion_backfill/dagRuns"
)
