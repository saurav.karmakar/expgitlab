package main

type AirFlowExecutorImpl struct {
	ExeMetadata *ExecutionMetadata
}

type OrgWiseTriggerRes struct {
	OrgId      string                      `json:"orgId"`
	TriggerRes TriggerUsageBackfillResBody `json:"triggerRes"`
	Error      string                      `json:"error"`
}

type ConsolidatedUsageBackfillRes struct {
	Result []OrgWiseTriggerRes `json:"result"`
}

func (afei *AirFlowExecutorImpl) TriggerUsageBackfill(triggerReq TriggerUsageBackfillReqBody) ConsolidatedUsageBackfillRes {
	res := ConsolidatedUsageBackfillRes{}
	for _, orgId := range triggerReq.Orgs {
		logInfo("Triggering for org: " + orgId)

		orgWiseRes, err := triggerDAGWithConfig(afei, orgId, triggerReq.Months, triggerReq.Cloud)

		if err != nil {
			// Setting up the object even though there is error
			res.Result = append(res.Result, OrgWiseTriggerRes{
				OrgId:      orgId,
				TriggerRes: orgWiseRes,
				Error:      err.Error(),
			})
		} else {
			// Setting up the object even though there is error
			res.Result = append(res.Result, OrgWiseTriggerRes{
				OrgId:      orgId,
				TriggerRes: orgWiseRes,
				Error:      "",
			})
		}

	}
	return res
}

func (afei *AirFlowExecutorImpl) ListAllDAGs() (DAGSList, error) {
	return listAllDAGS(afei)
}
