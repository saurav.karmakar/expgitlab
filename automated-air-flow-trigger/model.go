package main

import "encoding/json"

type ExecutionMetadata struct {
	Env         string `json:"env"`
	AirFlowHost string `json:"airFlowHost"`
	Session     string `json:"session"`
}

type DAGSList struct {
	Dags []DAGS `json:"dags"`
}

type DAGS struct {
	DAGId string `json:"dag_id"`
}

type Health struct {
	Version      string            `json:"version"`
	Message      string            `json:"message"`
	ExecMetadata ExecutionMetadata `json:"execMetadata"`
}

type TriggerUsageBackfillReqBody struct {
	Orgs   []string `json:"orgs"`
	Months int      `json:"months"`
	Cloud  string   `json:"cloud"`
}

type TriggerUsageBackfillResBody struct {
	Conf     Conf   `json:"conf"`
	DAGId    string `json:"dag_id"`
	DAGRunId string `json:"dag_run_id"`
	RunType  string `json:"run_type"`
	State    string `json:"state"`
}

type Conf struct {
	OrgId  string `json:"org_id"`
	Months int    `json:"months"`
	Cloud  string `json:"cloud"`
}

type Error struct {
	StatusCode int
	Message    string
}

func SetErrorResponse(statusCode int, message string) []byte {
	errorResp := &Error{
		StatusCode: statusCode,
		Message:    message,
	}
	data, _ := json.Marshal(errorResp)
	return data
}
