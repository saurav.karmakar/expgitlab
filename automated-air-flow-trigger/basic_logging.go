package main

import "fmt"

func logInfo(mesg string) {
	fmt.Println("[INFO] ", mesg)
}

// Future use
// func logDebug(mesg string) {
// 	fmt.Println("[DEBUG] ", mesg)
// }

func logError(mesg string) {
	fmt.Println("[ERROR] ", mesg)
}
