package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

func listAllDAGS(afei *AirFlowExecutorImpl) (DAGSList, error) {
	dagsLst := DAGSList{}

	// Cookie map
	cookies := map[string]string{
		"Cookie": afei.ExeMetadata.Session,
	}

	// Using the new call
	data, err := makeRequest(http.MethodGet, fmt.Sprintf(ALL_DAGS, afei.ExeMetadata.AirFlowHost), bytes.NewBuffer([]byte{}), cookies)

	if err != nil {
		return dagsLst, err
	}

	err = json.Unmarshal(data, &dagsLst)
	if err != nil {
		return dagsLst, err
	}

	return dagsLst, nil
}

func triggerDAGWithConfig(afei *AirFlowExecutorImpl, orgId string, months int, cloud string) (TriggerUsageBackfillResBody, error) {
	triggerUsageBackfillReqBody := TriggerUsageBackfillResBody{}

	// Declaring the post data
	dateTime, _ := time.Now().UTC().MarshalText()
	var jsonData = []byte(fmt.Sprintf(`{
		"dag_run_id": "manual-org-%s-%s" ,
		"conf": {
			"cloud": "%s",
			"months": %d,
			"org_id": "%s"
		}
	}`, orgId, dateTime, cloud, months, orgId))

	// Cookie map
	cookies := map[string]string{
		"Cookie": afei.ExeMetadata.Session,
	}

	// Using the new call
	data, err := makeRequest(http.MethodPost, fmt.Sprintf(TRIGGER_DAG, afei.ExeMetadata.AirFlowHost), bytes.NewBuffer(jsonData), cookies)

	if err != nil {
		return triggerUsageBackfillReqBody, err
	}

	err = json.Unmarshal(data, &triggerUsageBackfillReqBody)
	if err != nil {
		return triggerUsageBackfillReqBody, err
	}

	return triggerUsageBackfillReqBody, nil
}
