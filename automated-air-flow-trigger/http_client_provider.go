package main

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
)

// Generic HTTP method to make all HTTP request
func makeRequest(method string, url string, reqBody *bytes.Buffer, cookie map[string]string) ([]byte, error) {
	// Declare http client
	client := &http.Client{}

	// Declare HTTP Method and Url
	req, err := http.NewRequest(method, url, reqBody)
	if err != nil {
		return []byte{}, err
	}

	// Set cookie
	for k, v := range cookie {
		req.Header.Set(k, v)
	}
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")

	resp, err := client.Do(req)
	if err != nil {
		return []byte{}, err
	}
	if resp.StatusCode != 200 {
		bytes, _ := io.ReadAll(resp.Body)
		return []byte{}, fmt.Errorf("request failed, status code: %d, err: %s", resp.StatusCode, string(bytes))
	}
	defer resp.Body.Close()

	// Read response
	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return []byte{}, nil
	}

	return data, nil
}
